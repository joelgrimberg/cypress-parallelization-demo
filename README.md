# Cypress parallelization demo

## Demo

- [ ] Beginnen met cypress dashboard
- [ ] Hoe maak je project
- [ ] Hoe link je je project
- [ ] Draai het eerst lokaal
- [ ] Dan in ci brengen
- [ ] Resultaten zien
- [ ] Misschien pull request validatie
- [ ] En sorry cypress dashboard voor de cheap people

## Setup

- [ ] npm init -y
- [ ] npm install cypress --save-dev
- [ ] startup cypress & create kitchensink examples
- [ ] create npm run ci - command
- [ ] create dashboard key within Cypress dashboard
- [ ] add Cypress Dashboard Key to Gitlab CI/CD environment
- [ ] add Cypress Dashboard Key - key to npm run script
- [ ] add npm run script to gitlab-ci.yml
- [ ] add `parallel: 2` to npm run - script
